package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                this.guild.add(this);

        }

        //ToDo: Complete Me

        @Override
        public void update() {
                Quest quest = this.guild.getQuest();
                String type = this.guild.getQuestType();

                if(type.equals("Delivery") || type.equals("Rumble") ||type.equals("R") || type.equals("D")){
                        this.getQuests().add(quest);
                }

        }
}

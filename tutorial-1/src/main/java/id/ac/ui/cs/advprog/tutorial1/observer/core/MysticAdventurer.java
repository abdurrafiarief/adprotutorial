package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                this.guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                Quest quest = this.guild.getQuest();
                String type = this.guild.getQuestType();

                if(type.equals("Delivery") || type.equals("Escort") ||type.equals("E") || type.equals("D")){
                        this.getQuests().add(quest);
                }
        }
}

package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
    public AttackWithMagic(){}

    public String attack(){
        return "Attack with a blasting inferno fireball stream Avada Kedavra Alakazam";
    }

    @Override
    public String getType() {
        return "AttackWithMagic";
    }
}

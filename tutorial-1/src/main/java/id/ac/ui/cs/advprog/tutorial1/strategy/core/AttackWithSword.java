package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me
    public AttackWithSword(){}

    public String attack(){
        return "Attack with the Excalibur Sword that is 69 inches long";
    }

    public String getType(){
        return "AttackWithSword";
    }
}

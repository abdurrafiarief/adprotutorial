package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me
    public DefendWithArmor(){}

    @Override
    public String getType() {
        return "DefendWithArmor";
    }

    @Override
    public String defend() {
        return "My Armor Stat is over 9000!!!!";
    }
}
